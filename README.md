# bruno vue-project

Project based on https://www.youtube.com/watch?v=4deVCNJq3qc

with cool components from here: https://bootstrap-vue.org/

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
